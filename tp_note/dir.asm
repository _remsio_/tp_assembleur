.386
.model flat,stdcall
option casemap:none

; dernier argument : premier push

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc


; Liste des librairies utilis�es
includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
; db : declared byte
; , declarer plusieurs bytes
; le 0 doit �tre ajout� � la fin (nullbyte)
path     db    "C:/Users/MATASSE/Desktop/ensibs/tp_assembleur/tp_note",0
path2     db    "test/*",0
endPath     db    "/*",0
slash     db    "/",0
pointPoint  db	  "..",0
repertoireCourrant db "./*",0
emptyFileName  db	 "C:/Users/MATASSE/Desktop/ensibs/tp_assembleur/tp_note", 0


strCommand db	 "Pause...",10,0
printFileName db	 "(%d) <FILE>  %s",10,0
printDirectoryName db	 "(%d) <DIR> : %s",10,0
printError		db		"Code erreur : %d",10,0
printRecursive		db		"Quel repertoire voulez vous lister recursivement ? ",0


EntreeUtilisateurPaterne   db "%s",0
EntreeUtilisateur   db "                                                                                                                                                                                                                                                ", 0

Tabulation db "[*]",0

.DATA?
; variables non-initialisees (bss)

fichier WIN32_FIND_DATA <>

;typedef struct _FILETIME {
;  DWORD dwLowDateTime;
;  DWORD dwHighDateTime;
;} FILETIME, *PFILETIME, *LPFILETIME;

FileTime STRUCT
	dwLowDateTime	dd	?
	dwHighDateTime	dd	?
FileTime ENDS

;typedef struct _BY_HANDLE_FILE_INFORMATION {
;  DWORD    dwFileAttributes;
;  FILETIME ftCreationTime;
;  FILETIME ftLastAccessTime;
;  FILETIME ftLastWriteTime;
;  DWORD    dwVolumeSerialNumber;
;  DWORD    nFileSizeHigh;
;  DWORD    nFileSizeLow;
;  DWORD    nNumberOfLinks;
;  DWORD    nFileIndexHigh;
;  DWORD    nFileIndexLow;
;} BY_HANDLE_FILE_INFORMATION, *PBY_HANDLE_FILE_INFORMATION, *LPBY_HANDLE_FILE_INFORMATION;

FindData STRUCT
	dwFileAttributes	dd	?
	ftCreationTime	FileTime	<>
	ftLastAccessTime	FileTime	<>
	ftLastWriteTime	FileTime	<>
	nFileSizeHigh	dd	?
	nFileSizeLow	dd	?
	dwReserved0	dd	?
	dwReserved1	dd	?
	cFileName	db	260	dup(?)
	cAlternateFileName	db	14 dup(?)
	dwFileType	dd	?
	dwCreatorType	dd	?
	wFinderFlags	dw	?
FindData ENDS

fhandle	FindData	<>

.CODE


; retourne 0 s'il s'agit d'un fichier, sinon 1 s'il s'agit d'un repertoire
FileOrDirectory:
	push ebp ; save base pointer
	mov ebp, esp ; load stack into base pointer
	
	mov esi, [ebp+8]
	mov eax, [ebp+8]
	;FILE_ATTRIBUTE_DIRECTORY = 16
	cmp eax, 16
	je isDirectory
	jne Fin
	isDirectory:
		mov eax, 0
		mov esp, ebp
		pop ebp
		ret
	Fin:
	mov eax, 1
	mov esp, ebp
	pop ebp
	ret
	
; Gestion des erreurs
AfficherErreur:
	push ebp ; save base pointer
	mov ebp, esp ; load stack into base pointer
	
	mov esi, [ebp+8]
	push [ebp+8]
	push offset printError
	; call printf equivalent linux
	call crt_printf
	invoke crt_system, offset strCommand
	mov eax, 1
	invoke	ExitProcess,eax
	mov esp, ebp
	pop ebp
	ret


;[ebp -4] --> Compteur pour ne pas afficher . et ..
;[ebp -8] --> handle de base
;[ebp-12] --> nouveau handle
;[ebp-16] --> Repertoire courrant
;[ebp-20] --> compteur tabulation
PrintAllFromFolder:
	push ebp ; save base pointer
	mov ebp, esp ; load stack into base pointer
	sub esp,20 ; sauve 20 bytes pour les variables locals
	
	mov eax, [ebp+16]
	mov [ebp-20], eax
	
	mov eax, [ebp+12]
	mov [ebp-16], eax
	
	push [ebp-16]
	call SetCurrentDirectory
	
	;mov BYTE PTR [ebp -4], offset fichier
	
	;sauvegarde du handle
	mov eax, offset fichier
	mov [ebp -8], eax
	
	push [ebp -8]
	push offset repertoireCourrant
	call FindFirstFile ; on appel HANDLE FindFirstFile(string path, WIN32_FIND_DATA fichier)
	mov [ebp -12] , eax ; on r�cup�re le handle
	
	;compteur pour ne pas afficher . et ..
	mov BYTE PTR [ebp -4], 0
	
	whileFichier:
		
		;https://docs.microsoft.com/fr-fr/windows/desktop/FileIO/file-attribute-constants
		
		cmp BYTE PTR [ebp -4], 2
		jl finIfElsePrintFichier
		
		mov ebx, [ebp-20]
		
		; Boucle de tabulation
		cmp ebx, 0
		je finBoucleTabulation
		printTabulation:
			dec ebx
			push offset Tabulation
			call crt_printf
			cmp ebx, 0
			jg printTabulation
		finBoucleTabulation:
		
		push fichier.dwFileAttributes
		call FileOrDirectory
		cmp eax, 0
		jne elseDirectory
		
		push offset fichier.cFileName
		push fichier.dwFileAttributes
		push offset printDirectoryName
		; call printf equivalent linux
		call crt_printf
		
		mov eax, [ebp+16]
		inc eax
		
		push eax
		push offset fichier.cFileName
		push offset fichier
		call PrintAllFromFolder
		
		jmp finIfElsePrintFichier
	
	elseDirectory:
	
		push offset fichier.cFileName
		push fichier.dwFileAttributes
		push offset printFileName
		; call printf equivalent linux
		call crt_printf
	
	finIfElsePrintFichier:
		add BYTE PTR [ebp -4], 1
		
		push offset fichier
		push [ebp -12]
		call FindNextFile
		
		cmp eax, 0; On v�rifie qu'il y ait effectivement une erreur
		jne whileFichier
		
		call GetLastError
		
		cmp eax, 18; Si le code d'erreur est 18 (dernier fichier atteint)
		je dernierFichierAtteint
		
		push eax
		call AfficherErreur
	
	dernierFichierAtteint:
	push offset pointPoint
	call SetCurrentDirectory
	
	mov esp, ebp
	pop ebp
	ret

;Fonction d'entr�e
start:
	
	; Saisie utilisateur
	push offset printRecursive
	call crt_printf
	
	push offset EntreeUtilisateur
	push offset EntreeUtilisateurPaterne
	call crt_scanf
	
	push offset EntreeUtilisateur
	call SetCurrentDirectory
	
	xor eax,eax
	push eax
	push offset EntreeUtilisateur
	push offset fichier
	call PrintAllFromFolder
	
	invoke crt_system, offset strCommand
	mov eax, 0
	invoke	ExitProcess,eax
	
	
end start



