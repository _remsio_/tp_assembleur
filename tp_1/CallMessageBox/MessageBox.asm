.386
.model flat,stdcall
option casemap:none

; dernier argument : premier push

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc


; Liste des librairies utilisées
includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
; db : declared byte
; , declarer plusieurs bytes
; le 0 doit être ajouté à la fin (nullbyte)
Phrase     db    "Hello World !",10,0
strCommand db "Titre",13,10,0

.DATA?
; variables non-initialisees (bss)

.CODE
start:
		; on place le premier argument de la fonction appelée sur la pile
		push MB_YESNO
		; On place le second argument de la fonction appelée sur la pile
        push offset strCommand
		push offset Phrase
		push NULL
        ; call printf equivalent linux
        call MessageBox
		;ou
		; equivalent push push call permet de laisser le terminal ouvert
		invoke MessageBox, NULL, addr Phrase, addr strCommand, MB_YESNO
		mov eax, 0
		; Permet de terminer correctement le programme
		; Il faut nettoyer la memoire : esp+8 / add esp,8
	    invoke	ExitProcess,eax

end start

