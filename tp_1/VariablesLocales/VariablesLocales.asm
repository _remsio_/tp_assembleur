.386
.model flat,stdcall
option casemap:none

; dernier argument : premier push

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc


; Liste des librairies utilisées
includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
; db : declared byte
; , declarer plusieurs bytes
; le 0 doit être ajouté à la fin (nullbyte)


.DATA?
; variables non-initialisees (bss)

.CODE

; Fonction retournant le nombre de caractere d'une chaine de caracteres
myst PROC
	push ebp ; save base pointer
	mov ebp, esp ; load stack into base pointer
	sub esp,20 ; sauve 10 bytes pour 5 variables
	mov BYTE PTR [ebp-20], 1 ; j
	mov BYTE PTR [ebp-16], 1 ; k
	mov BYTE PTR [ebp-12], 3 ; i
	mov eax, [ebp+8]
	mov BYTE PTR [ebp-8], al ; l
		
	mov esp, ebp
	pop ebp
	ret
myst ENDP
	
start:
	
	push 5
	call myst
	
	
end start



