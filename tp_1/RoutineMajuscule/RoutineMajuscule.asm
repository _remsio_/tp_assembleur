.386
.model flat,stdcall
option casemap:none

; dernier argument : premier push

include c:\masm32\include\windows.inc
include c:\masm32\include\gdi32.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc


; Liste des librairies utilisées
includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
; db : declared byte
; , declarer plusieurs bytes
; le 0 doit être ajouté à la fin (nullbyte)
Phrase     db    "coucou je vais passer en majuscule",10,0
strCommand     db    "Pause",10,0
printInteger db "%d",10,0

.DATA?
; variables non-initialisees (bss)

.CODE

; Fonction retournant le nombre de caractere d'une chaine de caracteres
fonctionCompterCaracteres PROC
	push ebp ; save base pointer
	mov ebp, esp ; load stack into base pointer
	sub esp,8 ; sauve 8 bytes
		
	xor ecx,ecx   ; On met le registre c à 0
	loopCompteur:
		mov ESI, [ebp+8]
		mov al, byte ptr [esi + ecx]
		inc ecx      ; On increment c
		cmp al, 0    ; On regarde si le caractere est un null byte
		je sortieLoopCompteur ; on sort de la boucle
		jmp loopCompteur
	sortieLoopCompteur:
	
	mov eax, ecx ; on retourne la valeur d'ecx (ret retourne dans eax)
	mov esp, ebp
	pop ebp
	ret
fonctionCompterCaracteres ENDP

; Fonction prenant une chaine en entrée
fonctionMajuscule PROC
	
	push ebp ; save base pointer
	mov ebp, esp ; load stack into base pointer
	sub esp,8 ; sauve 8 bytes
	
	xor ecx,ecx   ; On met le registre c à 0
	loop1:
		 mov ESI, [ebp+8]
		 mov al, byte ptr [esi + ecx]
		 cmp al, 97
		 jl test1
		 cmp al, 122
		 jg test1
		 sub al, 32
		 mov byte ptr [esi + ecx], al
		 test1: ;Ignore la partie sub si pas minuscule
		
	inc ecx      ; On increment c
	cmp al, 0    ; On compare cx au nombre d'iterations
	je sortieLoop1 ; on sort de la boucle
	jmp loop1   ; on retourne au niveau de loop1
	
	sortieLoop1: ; Quand on arrive 
	
	mov esp, ebp
	pop ebp
	ret

fonctionMajuscule ENDP
	
start:
		push offset Phrase
        ; call printf equivalent linux
        call crt_printf
		
		;Lire octet par octet
		
		push offset Phrase
		call fonctionMajuscule
		
		push offset Phrase
        ; call printf equivalent linux
        call crt_printf
		
		push offset Phrase
		call fonctionCompterCaracteres
		
		push eax
		push offset printInteger
        ; call printf equivalent linux
        call crt_printf
		
		
		invoke crt_system, offset strCommand
		mov eax, 0
		; Permet de terminer correctement le programme
		; Il faut nettoyer la memoire : esp+8 / add esp,8
	    invoke	ExitProcess,eax
end start



